/**
 * Proyecto para examen de admisión
**/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define CANTIDAD_MAXIMA_DE_NUMEROS 20

void numerosDivisiblesEntre3(){
    int contador = 1;
    int contadorDeNumerosDivisiblesEntre3 = 0;
    int *numerosDivisiblesEntre3 = malloc(sizeof(int) * CANTIDAD_MAXIMA_DE_NUMEROS);

    // Leer numeros
    while(contadorDeNumerosDivisiblesEntre3 < CANTIDAD_MAXIMA_DE_NUMEROS){
        if (contador % 3 == 0){
            *(numerosDivisiblesEntre3 + contadorDeNumerosDivisiblesEntre3) = contador;
            contadorDeNumerosDivisiblesEntre3 += 1;
        }
        contador += 1;
    }

    // Imprimir los numeros
    contador = 0;
    while(contador < CANTIDAD_MAXIMA_DE_NUMEROS){
        printf("\n%d", *(numerosDivisiblesEntre3 + contador));
        contador += 1;
    }
}

int main() {
    numerosDivisiblesEntre3();
    return 0;
}