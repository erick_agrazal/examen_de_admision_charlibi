/**
 * Proyecto para examen de admisión
**/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void calcularMenorElementoDeMatriz(){
    // Declaración de variables
    int x = 0, y = 0, filasMatriz, columnasMatriz, **matriz, elementoMasPequeno;

    // Leyendo tamaño de matriz
    printf("Introduzca el filas de la matriz: ");
    scanf("%d", &filasMatriz);
    printf("\nIntroduzca el columnas de la matriz: ");
    scanf("%d", &columnasMatriz);

    // Separando espacio de memoria de la matriz
    matriz = (int **) malloc (filasMatriz * sizeof(int *));
    for (x=0; x < filasMatriz; x++)
        matriz[x] = (int *) malloc (columnasMatriz * sizeof(int));

    // Leyendo valores del stdin y guardandolos en la matriz
    for (x=0; x < filasMatriz; x++){
        for (y=0; y < columnasMatriz; y++){
            printf("\nIntroduzca el valor de la fila %d y la columna %d: ", x, y);
            scanf("%d", *(matriz + x) + y);
        }
    }

    // Calculando el elemento más pequeño de la matriz
    for (x=0; x < filasMatriz; x++){
        for (y=0; y < columnasMatriz; y++){
            if(x == 0 && y == 0){
                elementoMasPequeno = *(*(matriz + x) + y);
            } else if (*(*(matriz + x) + y) < elementoMasPequeno){
                elementoMasPequeno = *(*(matriz + x) + y);
            }
        }
    }

    // Imprimiendo el elemento mas pequeño
    printf("\nEl elemento más pequeño es: %d", elementoMasPequeno);
}

int main(){
    calcularMenorElementoDeMatriz();
    return 0;
}