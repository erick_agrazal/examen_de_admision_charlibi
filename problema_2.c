/**
 * Proyecto para examen de admisión
**/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>


void calcularMenorElemento(){
    int contador = 0;
    int elementoMasPequeno = 0;

    // Cantidad de elementos del array
    int cantidadDeElementos;
    printf("Introduce el tamaño del arreglo: ");
    scanf("%d", &cantidadDeElementos);

    // Leer elementos del array
    int *elementos = malloc(sizeof(int) * cantidadDeElementos);
    while(contador < cantidadDeElementos){
        printf("\nIntroduzca el valor #%d: ", contador + 1);
        scanf("%d", elementos + contador);
        contador += 1;
    }

    // Calcular menor
    contador = 0;
    while(contador < cantidadDeElementos){
        if(contador == 0){
            elementoMasPequeno = *(elementos + contador);
        } else if (*(elementos + contador) < *(elementos + contador - 1)) {
            elementoMasPequeno = *(elementos + contador);
        }
        contador += 1;
    }

    // Imprimir resultado
    printf("\nEl elemento más pequeño es: %d", elementoMasPequeno);
}

int main() {
    calcularMenorElemento();
    return 0;
}