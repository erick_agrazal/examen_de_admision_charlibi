/**
 * Proyecto para examen de admisión
**/

#include <stdio.h>
#include <math.h>


int calcularSumatoria(int x, int max){
    int contador = 1;
    int sign = 1;
    int acumulador = 0;
    while(contador <= max){
        printf("\nEjecutando operación %d * %d * %d ** %d", sign, contador, x, contador - 1);
        acumulador += (sign) * (contador) * pow(x, contador - 1);
        sign *= -1;
        contador +=1;
    }
    return acumulador;
}


int main() {
    // printf() displays the string inside quotation
    printf("\nThe S is: %d", calcularSumatoria(2, 10));
    return 0;
}