Build and run the Docker image:

```
$ docker build -t erickagrazal/gcc:latest .
$ docker run -it --rm --name running-app erickagrazal/gcc:latest ./problema_1
```