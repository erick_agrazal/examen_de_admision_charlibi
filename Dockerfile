FROM gcc:9.2.0
COPY . /usr/src/app
WORKDIR /usr/src/app
RUN gcc -o problema_1 /usr/src/app/problema_1.c -lm
RUN gcc -o problema_2 /usr/src/app/problema_2.c -lm
RUN gcc -o problema_3 /usr/src/app/problema_3.c -lm
RUN gcc -o problema_5 /usr/src/app/problema_5.c -lm
# CMD ["./problema_1"]